@echo off
call m.bat
if not errorlevel 1 (
    rem Run nex file directly in CSpect
    if not exist _temp\enNextZX.rom (
        copy env\*.rom _temp
        rem env\hdfmonkey\hdfmonkey put _temp\tbblue.mmc env\enAltZX.rom /machines/next
    )
    env\cspect\CSpect.exe -basickeys -r -tv -brk -16bit -s28 -w3 -zxnext -nextrom -map=ned.map -mmc=_temp\tbblue.mmc
)
