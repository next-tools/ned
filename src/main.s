;;----------------------------------------------------------------------------------------------------------------------
;; Yokai
;; Copyright (C)2020 Matt Davies, all right reserved.
;;----------------------------------------------------------------------------------------------------------------------

                DEVICE          ZXSPECTRUMNEXT
                CSPECTMAP       "ned.map"

;;----------------------------------------------------------------------------------------------------------------------
;; Includes

                include "console/consts.s"
                include "console/defines.s"
                include "console/keyboard.s"

                include "app/app.s"
                include "console/console.s"
                include "console/dot.s"
                include "console/memory.s"
                include "console/utils.s"

;;----------------------------------------------------------------------------------------------------------------------
;; Start

Start:
                ld      (OldSP),sp
                ld      sp,$4000

                ;;
                ;; Handle arguments
                ;;

                ld      de,Scratch
                call    getArg
                call    dotStart

                ;;
                ;; INITIALISATION
                ;;

                call    consoleInit
                call    cursorOn
                call    appInit
                call    keysInit

                ld      a,(Scratch)
                and     a
                call    z,appNewDoc
                call    nz,appLoadDoc

                ;;
                ;; MAIN LOOP
                ;;

.main_loop:
                call    consoleUpdate

                call    inKey
                call    appUpdate
                jr      z,.main_loop

                cp      VK_EXTQ
                jr      nz,.main_loop


                ;;
                ;; EXIT
                ;;

Exit:
                call    keysDone
                call    appDone
                call    consoleDone
                jp      dotEnd

;;----------------------------------------------------------------------------------------------------------------------
;; getRasterLine
;; Get the current raster line in HL
;;
;; Output:
;;      HL = raster line #
;;

getRasterLine:
                push    af,bc

                ld      bc,IO_REG_SELECT
                ld      a,REG_VLINE_L
                out     (c),a
                inc     b
                in      l,(c)

                dec     b
                dec     a
                out     (c),a
                inc     b
                in      h,(c)           ; HL = raster count

                pop     bc,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; waitLine
;; Wait for a particular line
;;
;; Input:
;;      DE = Raster line # to wait for
;;

waitLine:
                push    af,de

.l0:
                call    getRasterLine   ; DE = raster line
                call    compare16       ; HL == DE?
                jr      nz,.l0          ; No?  Keep waiting

                pop     de,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; waitNotLine
;; Wait for the raster line not be equal to DE.
;;
;; Input:
;;      DE = Raster line # to wait for
;;

waitNotLine:
                push    af,de

.l0:
                call    getRasterLine   ; HL = raster line
                call    compare16       ; HL == DE?
                jr      z,.l0           ; No?  Keep waiting

                pop     de,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; End of code

                display "[MAIN] ",$,"/0x3d00: (",$3d00-$,")"

;;----------------------------------------------------------------------------------------------------------------------
;; Stack & Data

                org     $3d00
Scratch:        ds      256             ; Scratch buffer
KeyboardBuffer: ds      256             ; Keyboard buffer
                ds      256
stack_top:

;;----------------------------------------------------------------------------------------------------------------------
;; Data

data_start:
                INCBIN  "data/font.bin"
data_end:

;;----------------------------------------------------------------------------------------------------------------------
;; File generation

                SAVEBIN "ned", $2000, $2000 + (data_end - data_start)

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
