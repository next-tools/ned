;;----------------------------------------------------------------------------------------------------------------------
;; Console emulation for Ned
;;
;; Functions:
;;      consoleInit             Initialise the console system (set video mode etc)
;;      consoleDone             Shut-down console system
;;      consoleUpdate           Call once a frame to render/flash cursor
;;      modeConsole             Switch to console video mode
;;      modeULA                 Switch to ULA mode
;;      cls                     Clear the screen and reset cursor position
;;      at                      Set cursor position
;;      scrAddr                 Calculate screen address and get current position
;;      calcAddr                Calculate screen address for a given position
;;      setColour               Set the current text colour
;;      cursorOn                Turn on the cursor
;;      cursorOff               Turn off the cursor
;;      cursorHide              Temporarily hide the cursor before doing operations that move the cursor
;;      cursorShow              Restore the cursor after hiding it
;;      consoleStore            Store current state of console
;;      consoleRestore          Restore state of console previously saved by consoleStore
;;
;;      print                   Print a null-terminated string following the call
;;      colouredPrint           As 'print' but set colour
;;      errorPrint              Print using kInkError colour and set carry flag
;;      printHL                 Print null-terminated string at HL
;;      printHLWidth            As printHL but limit to a particular width
;;      printChar               Print a single character
;;      printHexDigit           Print single hex digit from lower nybble in A
;;      printHexByte            Print 2 digit hex value
;;      printHexWord            Print 4 digit hex value
;;      
;;----------------------------------------------------------------------------------------------------------------------

ConsolePages    db      0, 0
OrigBorder      db      0

;;----------------------------------------------------------------------------------------------------------------------
;; Special character codes
;;----------------------------------------------------------------------------------------------------------------------

C_LEFT          equ     VK_LEFT         ; Move cursor left
C_RIGHT         equ     VK_RIGHT        ; Move cursor right
C_DOWN          equ     VK_DOWN         ; Move cursor down
C_UP            equ     VK_UP           ; Move cursor up
C_ENTER         equ     VK_ENTER        ; Move cursor to beginning of next line
C_BACKSPACE     equ     VK_DELETE       ; Delete previous character
C_DELETE        equ     VK_TRUEVIDEO    ; Delete character under cursor
C_CLS           equ     VK_EXTL         ; Clear whole screen
C_CLEARLINE     equ     $80+'1'         ; Clear whole current line
C_TAB           equ     VK_INVVIDEO     ; Insert TAB
C_OVERWRITE     equ     VK_EXTO         ; Toggle overwrite
C_HOME          equ     $80+'5'         ; Move to start of current line
C_END           equ     $80+'8'         ; Move to end of current line
C_CLEARTOEND    equ     VK_EDIT         ; Clear from current position to end of current line

;;----------------------------------------------------------------------------------------------------------------------
;; Console state
;;----------------------------------------------------------------------------------------------------------------------

CurrentCoords   dw      0               ; YX coords of cursor position
CurrentPos      dw      0               ; Tile address of cursor position

CurrentColour   db      %0'000'0'111 << 1

CursorFlag      db      0               ; Bit 0 = Enable, 1 = Visible, 2 = Hidden
CursorColour    db      %0'100'1'111    ; Current cursor colour
CursorBack      db      0               ; Saved attribute when cursor is drawn

CONSOLE_STATE   equ     $ - CurrentCoords       ; Length of state

ConsoleState    ds      CONSOLE_STATE   ; Space for storage from consoleStore

;;----------------------------------------------------------------------------------------------------------------------
;; consoleInit
;; Initialise the video mode, load the font, set up the palette and clear the screen.
;;----------------------------------------------------------------------------------------------------------------------

consoleInit:
                ld      a,($5c48)
                ld      (OrigBorder),a

                page    2,10
                page    3,11

                ; Backup MMU 2 & 3
                call    allocPage
                ld      (ConsolePages+0),a
                page    4,a
                call    allocPage
                ld      (ConsolePages+1),a
                page    5,a

                ld      hl,$4000
                ld      de,$8000
                ld      bc,$4000
                call    memcpy

                ; Load in font
                dos     M_GETHANDLE     ; A = file handle to dot command
                ld      hl,$6000        ; Tiles data
                ld      bc,$800         ; 1K of data
                dos     F_READ          ; Read in the font data

                call    modeConsole

                ret

modeConsole:
                ; Set the border back
                xor     a
                out     ($fe),a

                ; Set up the tilemap
                reg     REG_TMAP_CTL,%11001011           ; Tilemap control: Enable, 80x32, Attrs, 1st palette,
                                                         ;                  text mode, 512 tiles, tilemap over ULA
                reg     REG_TMAP_BASE,$00                ; Tilemap base offset: $4000-$4f00 (80*24*2)
                reg     REG_TILES_BASE,$20               ; Tiles base offset: $6000-$7000 (8*512)
                reg     REG_TMAP_TRANS,8                 ; Transparency colour (bright black)
                reg     REG_ULA_CTL,%10000000            ; Disable ULA output

                ; Reset scrolling and clip window
                xor     a
                reg     REG_CLIP_TMAP,a
                reg     REG_CLIP_TMAP,159
                reg     REG_CLIP_TMAP,a
                reg     REG_CLIP_TMAP,255
                reg     REG_TMAP_XMSB,a
                reg     REG_TMAP_XLSB,a
                reg     REG_TMAP_Y,a

                ; Clear the screen
                call    cls

                ; Initialise the tilemap palette
                reg     REG_PAL_CTL,%00110000           ; Set tilemap palette for editing
                reg     REG_PAL_INDEX,a                 ; Start at palette index 0
                ld      b,a                             ; B = index (0PPPIIII, P=paper, I=ink)
                ld      de,Palette                      ; The palette table for colours 0-15
.l1:
                ; Set the paper colour
                ld      a,b
                and     $70
                swap                    ; A = 00000PPP
                ld      hl,de
                add     hl,a
                ld      a,(hl)          ; Get palette entry (RRRGGBB)
                reg     REG_PAL_VAL8,a

                ; Set the ink colour
                ld      a,b
                and     $0f
                ld      hl,de
                add     hl,a
                ld      a,(hl)
                reg     REG_PAL_VAL8,a

                inc     b
                jp      p,.l1           ; Until bit 7 is set (i.e. 128 entries).

                ld      bc,0
                call    at

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Palette table

Palette:                        ;       R       G       B
                db  %00000000   ;       0       0       0       Black
                db  %00000010   ;       0       0       5       Blue
                db  %10100000   ;       5       0       0       Red
                db  %10100010   ;       5       0       5       Magenta
                db  %00010100   ;       0       5       0       Green
                db  %00010110   ;       0       5       5       Cyan
                db  %10110100   ;       5       5       0       Yellow
                db  %10110110   ;       5       5       5       Light grey
                db  %01101101   ;       3       3       3       Dark grey
                db  %00000011   ;       0       0       7       Bright blue
                db  %11100000   ;       7       0       0       Bright red
                db  %11100011   ;       7       0       7       Bright magenta
                db  %00011100   ;       0       7       0       Bright green
                db  %00011111   ;       0       7       7       Bright cyan
                db  %11111100   ;       7       7       0       Bright yellow
                db  %11111111   ;       7       7       7       White

;;----------------------------------------------------------------------------------------------------------------------
;; consoleDone
;; Restore the ULA mode
;;----------------------------------------------------------------------------------------------------------------------

consoleDone:
                ; Restore pages 10 and 11
                ld      a,(ConsolePages+0)
                page    4,a
                ld      a,(ConsolePages+1)
                page    5,a
                ld      hl,$8000
                ld      de,$4000
                ld      bc,$4000
                call    memcpy
                ld      a,(ConsolePages+0)
                call    freePage
                ld      a,(ConsolePages+1)
                call    freePage

                call    modeULA

                ret

modeULA:
                ; Disable tilemap and enable ULA
                xor     a
                reg     REG_ULA_CTL,a
                reg     REG_TMAP_CTL,a

                ; Restore border colour
                ld      a,(OrigBorder)
                rrca
                rrca
                rrca
                out     (IO_ULA),a

                ; Clear the screen
                page    2,10
                page    3,11
                ld      hl,$4000
                ld      bc,$1800
                xor     a
                call    memfill
                ld      hl,$5800
                ld      bc,768
                ld      a,(OrigBorder)
                and     $38
                call    memfill

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cls
;; Clear the screen
;;

cls:
                push    af,bc,hl
                ld      hl,$4000
                ld      bc,$2*80*32
                xor     a
                call    memfill
                pop     hl,bc,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; at
;; Sets the cursor position
;;
;; Input:
;;      BC = YX coord
;;
;; Output:
;;      HL = tile address
;;      CurrentPos set to tile address too
;;      A = tilemap attribute under this position
;;

at:
                call    cursorHide
                ld      (CurrentCoords),bc
                call    scrAddr
                ld      (CurrentPos),hl
                call    cursorShow
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; scrAddr
;; Get the tile address at the current cursor.
;;
;; Output:
;;      HL = tile address
;;      BC = current coords (YX)
;;

scrAddr:
                ld      bc,(CurrentCoords)

;;----------------------------------------------------------------------------------------------------------------------
;; calcAddr
;; Calculate address for given coords
;;
;; Input:
;;      BC = coords (YX)
;;
;; Output:
;;      HL = tile address
;;

calcAddr:
                push    de
                ld      e,b
                ld      d,80
                mul
                ex      de,hl
                ld      a,c
                add     hl,a
                add     hl,hl
                ld      de,$4000
                add     hl,de
                pop     de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; setColour
;; Set the current colour for printing
;;
;; Input:
;;      A = colour (0 PPP B III)        P = paper, B = bright (for ink only), I = ink
;;
;; Output:
;;      A = tilemap attribute for text mode
;;

setColour:
                add     a,a
                ld      (CurrentColour),a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Cursor control
;;
;; If enable = 1, the cursor can be shown.  If 0, visible should also be 0.
;; If visible = 1, the cursor is currently shown on the screen.
;; If hidden = 1, the cursor can not be rendered as it's moving.  Visible should also be 0 at this point.
;;

cursorDraw:
                ; Draw the cursor on the screen
                push    af

                ; Don't draw the cursor if disabled, visible or hidden
                ld      a,(CursorFlag)
                xor     %001
                jr      nz,.end

                push    hl
                
                call    scrAddr         ; HL = address of cursor
                inc     hl              ; HL = address of attribute
                ld      a,(hl)
                ld      (CursorBack),a
                ld      a,(CursorColour)
                add     a,a
                ld      (hl),a

                ld      a,(CursorFlag)
                or      2
                ld      (CursorFlag),a
                
                pop     hl
.end:
                pop     af
                ret

cursorRemove:
                ; Remove the cursor from the screen
                push    af

                ; Don't remove the cursor if disabled, not visible or hidden
                ld      a,(CursorFlag)
                xor     %011
                jr      nz,.end

                push    hl

                call    scrAddr         ; HL = address of cursor
                inc     hl              ; HL = address of attribute
                ld      a,(CursorBack)
                ld      (hl),a

                ld      a,(CursorFlag)
                and     ~2
                ld      (CursorFlag),a
                pop     hl
.end:
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cursorOn
;; Turn the cursor on
;;

cursorOn:
                push    af
                ld      a,(CursorFlag)
                or      1
                ld      (CursorFlag),a
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cursorOff
;; Turn the cursor off

cursorOff:
                push    af
                call    cursorRemove            ; Remove the cursor if it's there
                ld      a,(CursorFlag)
                and     ~1
                ld      (CursorFlag),a
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cursorHide
;; Remove the cursor from the screen to move it.  This does not affect it's show/hide state.

cursorHide:
                push    af

                ; If already hidden, just exit
                ld      a,(CursorFlag)
                cpl
                and     %100
                jr      z,.end

                ; Remove the cursor if showing.
                call    cursorRemove

                ; Mark as hidden
                ld      a,(CursorFlag)
                or      4
                ld      (CursorFlag),a
.end:
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cursorShow
;; Display the cursor and copy the attribute under the cursor - BE CAREFUL don't call showCursor while it us being
;; shown.

cursorShow:
                push    af

                ; If already showing, just exit
                ld      a,(CursorFlag)
                and     %100
                jr      z,.end

                ; Update the colour to remove the cursor
                push    hl
                call    scrAddr
                inc     hl
                ld      a,(hl)
                ld      (CursorBack),a
                pop     hl

                ; Update the flag
                ld      a,(CursorFlag)
                and     ~4
                ld      (CursorFlag),a
.end:
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; consoleUpdate
;; Update the cursor flashing

consoleUpdate:
                ; If the cursor is off, there's nothing to do
                ld      a,(Counter)
                and     16              ; Flash every 16 frames
                jp      nz,cursorDraw
                jp      cursorRemove

;;----------------------------------------------------------------------------------------------------------------------
;; colouredPrint
;;
;; Input:
;;      A = colour
;;

colouredPrint:
                call    setColour
                ex      (sp),hl
                call    printHL
                ex      (sp),hl
                ld      a,kInkNormal
                call    setColour
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; errorPrint
;; Print a message after the call as an error.  VK_EDIT is output to clear the line and the ink is set to the error 
;; colour.  Ink is restored afterwards to normal text.  Finally, VK_ENTER is outputted afterwards.
;;

errorPrint:
                call    pageVideo
                call    cursorHide
                ld      a,kInkError
                call    setColour
                ld      a,C_CLEARLINE
                call    printChar
                ex      (sp),hl
                call    printHL
                ex      (sp),hl
                ld      a,C_ENTER
                call    printChar
                ld      a,kInkNormal
                call    setColour
                scf
                jr      print.end

;;----------------------------------------------------------------------------------------------------------------------
;; print
;; Print the text after the call point

print:
                call    cursorHide
                ex      (sp),hl         ; HL = data to print
                call    printHL
                ex      (sp),hl         ; Restore HL and set return address after the data
.end:
                call    cursorShow
                ret

printHL:
                push    bc
                ld      b,80
                call    printHLWidth
                pop     bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; printHLWidth
;; Similar to printHL but only allows a maximum number of characters to be printed.
;;
;; Input:
;;      HL = null terminated text
;;      B = number of characters
;;

printHLWidth:
                push    af,bc,de

                ; Turn off insert mode
                ld      a,(InsertMode)
                ld      c,a
                ld      a,$ff
                ld      (InsertMode),a

                ld      a,b
                or      a
                jr      z,.end

.l1:
                ldi     a,(hl)
                and     a
                jr      z,.end          ; Finish if character is null
                call    printChar
                cp      C_ENTER
                jr      nz,.not_eol
                ld      b,80
.not_eol:
                cp      $21
                jr      c,.l1           ; Next character, don't count whitespace
                cp      $e1
                jr      nc,.l1
                djnz    .l1
.end:
                ; Restore insert mode
                ld      a,c
                ld      (InsertMode),a

                pop     de,bc,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; printChar
;; Print a character on the screen and handle control codes.  Ensure that cursor is not showing.  print will do this
;; for you.
;;

printChar:
                push    af,bc,de,hl
                call    .print_char
                pop     hl,de,bc,af
                ret

.print_char:
                cp      $b0
                jp      nc,.ctrlCode
                cp      ' '
                jp      c,.ctrlCode             ; Character is a control code, so deal with it

                ; Check insert mode (0 = insert, 1 = overwrite)
.type_char:
                push    af
                ld      a,(InsertMode)
                ld      hl,(CurrentPos)
                ld      bc,(CurrentCoords)
                add     a,a                     ; A->CF
                jr      c,.no_insert

                ; Shift the characters to the right if we're in insert mode
                pop     af
                call    moreRoom        ; Shift the characters on the line to the right
                push    af
.no_insert:
                pop     af
                ld      (hl),a          ; Draw character
                inc     hl
                ld      a,(CurrentColour)
                ld      (hl),a          ; And colour it
                dec     hl

.move_right:
                inc     hl
                inc     hl
                inc     c
                ld      a,c
                cp      kScreenWidth    ; Reached edge of screen?
                jr      nz,.update
                dec     c
                dec     hl
                dec     hl              ; Don't go past edge of screen
                ;inc     b
                ;ld      c,0
.test_end:
                ld      a,b
                cp      kScreenHeight   ; Reached end of screen?
                jr      nz,.update
                call    scrollUp
                ld      de,kScreenWidth*2
                sub     hl,de
                dec     b               ; Get the cursor back on the screen

.update:
                ld      (CurrentCoords),bc
                ld      (CurrentPos),hl
                ret
                
.ctrlCode:
                ; Deal with control codes
                ld      bc,(CurrentCoords)
                ld      hl,(CurrentPos)

                cp      C_RIGHT                 ; Right
                jr      z,.move_right
                cp      C_LEFT                  ; Left
                jr      z,.move_left
                cp      C_DOWN                  ; Down
                jr      z,.move_down
                cp      C_UP                    ; Up
                jr      z,.move_up
                cp      C_ENTER                 ; Enter
                jr      z,.enter
                cp      C_BACKSPACE             ; Delete backwards
                jr      z,.backspace
                cp      C_DELETE                ; Delete forwards
                jr      z,.delete
                cp      C_CLS                   ; CLS
                jr      z,.cls
                cp      C_CLEARLINE             ; Clear line
                jp      z,.clearLine
                cp      C_TAB                   ; Tab
                jp      z,.tab
                cp      C_OVERWRITE             ; Overwrite mode
                jp      z,.overwrite
                cp      C_HOME                  ; Home
                jr      z,.home
                cp      C_END                   ; End
                jr      z,.end
                cp      C_CLEARTOEND            ; Clear to end of line
                jp      z,.clearToEnd

                ; All unknown codes are ignored
                jr      .update

.enter:
                ; Go to beginning of line
                ld      e,c
                ld      d,0
                sub     hl,de
                sbc     hl,de
                ld      c,0

.move_down:
                inc     b
                ld      de,160
                add     hl,de
                jr      .test_end

.move_up:
                ld      a,b
                and     a
                jr      z,.update
                dec     b
                ld      de,160
                sub     hl,de
                jr      .update

.move_left:
                dec     hl
                dec     hl
                ld      a,c
                and     a
                jr      z,.ml_left              ; X == 0?
                dec     c
                jr      .update
.ml_left:
                ld      a,b
                and     a
                jr      z,.ml_top               ; Y == 0?
                dec     b
                ld      c,79                    ; Cursor at end of previous line
                jr      .update
.ml_top:          
                inc     hl      
                inc     hl                      ; Can't go past end of screen so restore position
                jp      .update


.delete:
                call    lessRoom
                jp      .update

.backspace:
                ; Make sure we're not on the left edge of the screen
                ld      a,c
                and     a
                jp      z,.update               ; We are?  No deleting for us!

                dec     c
                dec     hl
                dec     hl
                call    lessRoom
                jp      .update

.cls:
                call    cls
                ld      bc,0
                ld      hl,$4000
                jp      .update

.home:
                ld      a,c
                add     a,a
                ld      e,a
                ld      d,0
                and     a
                sbc     hl,de
                ld      c,0
                jp      .update

.end:
                ld      a,c
                sub     79
                neg
                add     a,a
                add     hl,a                    ; HL = end of line
                ld      c,79
                ld      a,(hl)
                and     a
                jp      nz,.update
.end_loop:
                ld      a,c
                and     a
                jp      z,.update
                ld      a,(hl)
                and     a
                jp      nz,.end_done
                dec     c
                dec     hl
                dec     hl
                jr      .end_loop
.end_done:
                inc     c
                inc     hl
                inc     hl
                jp      .update

.clearToEnd:
                ld      a,c
                sub     80
                neg
                add     a,a

                push    bc
                ld      c,a
                ld      b,0
                xor     a
                call    memfill
                pop     bc
                jp      .update

.clearLine:
                push    bc
                ld      b,0
                and     a
                sbc     hl,bc
                sbc     hl,bc                   ; HL = beginning of line

                ld      bc,160
                xor     a
                call    memfill

                pop     bc
                ld      c,0
                jp      .update

.tab:
                ld      a,' '
                call    .type_char
                ld      a,c
                and     3                       ; Reached next tab position
                jr      nz,.tab
                jp      .update

.overwrite:
                ld      a,(InsertMode)
                cpl
                ld      (InsertMode),a
                add     a,a
                jr      c,.cur_ovr
                ld      a,%0'100'1'111
                jr      .set_cur
.cur_ovr:       ld      a,%0'010'1'111
.set_cur:       ld      (CursorColour),a
                jp      .update

InsertMode      db      0

;;----------------------------------------------------------------------------------------------------------------------
;; moreRoom
;; Make more room on the line by shifting the characters to the right
;;
;; Input:
;;      HL = tilemap position
;;      C = X position
;;

moreRoom:
                push    af,bc,de,hl
                ld      a,80
                sub     c
                ld      c,a
                dec     c
                ld      b,0
                sla     c
                rl      b
                ld      de,hl
                inc     de
                inc     de
                call    memcpy_r
                pop     hl,de,bc
                xor     a
                ld      (hl),a          ; Clear current character
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; lessRoom
;; Shift the characters from the position to the left, inserting empty spaces on the right
;;
;; Input:
;;      HL = tilemap position
;;      C = X position
;;

lessRoom:
                push    af,bc,de,hl
                ld      a,80
                sub     c
                ld      c,a
                dec     c
                ld      b,0
                sla     c
                rl      b
                ld      de,hl
                inc     hl
                inc     hl
                call    memcpy
                add     hl,bc
                xor     a
                dec     hl
                ld      (hl),a
                dec     hl
                ld      (hl),a
                pop     hl,de,bc,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; scrollUp
;; Scroll the screen upwards
;;

scrollUp:
                push    af,bc,de,hl

                ; Move tiles up wards
                ld      hl,kScreenTop+(80*2)         ; Point to 2nd line
                ld      de,kScreenTop                ; Move to 1st line
                ld      bc,80*(kScreenHeight-1)*2    ; Move 31 lines
                call    memcpy
                ld      hl,kScreenTop+((kScreenHeight-1)*80*2)      ; HL = last line
                ld      bc,160
                xor     a
                call    memfill

                pop     hl,de,bc,af
                ret
 
;;----------------------------------------------------------------------------------------------------------------------
;; printHexDigit
;;
;; Input:
;;      A = nybble
;;

printHexDigit:
                ; Convert nybble to ASCII
                cp      10
                sbc     a,$69
                daa

                jp      printChar

;;----------------------------------------------------------------------------------------------------------------------
;; printHexByte
;;
;; Input:
;;      A = byte
;;

printHexByte:
                push    af
                swap
                and     $0f
                call    printHexDigit
                pop     af
                and     $0f
                jp      printHexDigit

;;----------------------------------------------------------------------------------------------------------------------
;; printHexWord
;;
;; Input:
;;      HL = word
;;

printHexWord:
                ld      a,h
                call    printHexByte
                ld      a,l
                jp      printHexByte

;;----------------------------------------------------------------------------------------------------------------------
;; consoleStore
;; Store the current state of the console for later restoration.  You CANNOT nest this.
;;

consoleStore:
                push    bc,de,hl

                ld      hl,CurrentCoords
                ld      de,ConsoleState
                ld      bc,CONSOLE_STATE
                call    memcpy

                pop     hl,de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; consoleRestore
;; Restore the previously saved state from consoleStore.
;;

consoleRestore:
                push    bc,de,hl

                call    cursorHide

                ld      hl,ConsoleState
                ld      de,CurrentCoords
                ld      bc,CONSOLE_STATE
                call    memcpy

                call    cursorShow
                pop     hl,de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------

