;;----------------------------------------------------------------------------------------------------------------------
;; Z80 and Next macros and registers
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; Pseudo-instructions

; Acts like: LD HL,(HL)
;
ldhl            macro
                ld      a,(hl)
                inc     hl
                ld      h,(hl)
                ld      l,a
                endm

ldde            macro
                ld      e,(hl)
                inc     hl
                ld      d,(hl)
                dec     hl
                endm

bchilo          macro   hi, lo
                ld      bc,(hi * 256) + lo
                endm

dehilo          macro   hi, lo
                ld      de,(hi * 256) + lo
                endm

hlhilo          macro   hi, lo
                ld      hl,(hi * 256) + lo
                endm

rreg            macro   reg
                ld      bc,IO_REG_SELECT
                ld      a,reg
                out     (c),a
                inc     b
                in      a,(c)
                endm

rreg2           macro   reg
                dec     b               ; Assumes BC = IO_REG_ACCESS from previous rreg call
                ld      a,reg
                out     (c),a
                inc     b
                in      a,(c)
                endm

callhl          macro
                push    $+5
                jp      (hl)
                endm

swap8           macro   r1,r2
                ld      a,r1
                ld      r1,r2
                ld      r2,a
                endm

swap16          macro   r1,r2
                push    r1
                push    r2
                pop     r1
                pop     r2
                endm

brka            macro   ch
                cp      ch
                jr      nz,$+4
                break
                endm

brkbc           macro   w
                ld      a,b
                cp      w/256
                jr      nz,$+9
                ld      a,c
                cp      w%256
                jr      nz,$+4
                break
                endm

reg             macro   r,n
                nextreg r,n
                endm

page            macro   slot, page
                nextreg $50+slot,page
                endm

res_cf          macro
                or      a
                endm

set_zf          macro
                cp      a
                endm

res_zf          macro
                or      1
                endm

set_sf          macro
                or      $80
                endm

res_sf          macro
                xor     a
                endm

set_even        macro
                xor     a
                endm

res_odd         macro
                sub     a
                endm

zf_to_cf        macro
                scf
                jr      z,$+3
                ccf
                endm

cf_to_zf        macro
                ccf
                sbc     a,a
                endm

stop            macro
                ld      a,2
                out     ($fe),a
                jr      $
                endm

;;----------------------------------------------------------------------------------------------------------------------
;; MMU addresses

addrMMU0        equ     $0000
addrMMU1        equ     $2000
addrMMU2        equ     $4000
addrMMU3        equ     $6000
addrMMU4        equ     $8000
addrMMU5        equ     $a000
addrMMU6        equ     $c000
addrMMU7        equ     $e000

;;----------------------------------------------------------------------------------------------------------------------
;; Opcode aliases

swap            macro
                swapnib
                endm

;;----------------------------------------------------------------------------------------------------------------------
;; Utility macros (interact with utilities.s)

regm            macro   register, mask, bits
                ld      a,register
                dehilo  ~mask, bits
                call    nregSet
                endm

;;----------------------------------------------------------------------------------------------------------------------
;; I/O Ports

; Communication and memory
IO_I2C_CLOCK            equ     $103b
IO_I2C_DATA             equ     $113b
IO_UART_TX              equ     $133b
IO_UART_RX              equ     $143b
IO_DMA                  equ     $6b

; Paging
IO_PLUS3_PAGE           equ     $1ffd
IO_128K_PAGE            equ     $7ffd
IO_NEXT_PAGE            equ     $dffd

; Next registers
IO_REG_SELECT           equ     $243b
IO_REG_ACCESS           equ     $253b

; Graphics
IO_LAYER2               equ     $123b
IO_SPRITE               equ     $303b
IO_SPRITE_ATTR          equ     $57
IO_SPRITE_PATT          equ     $5b
IO_ULA                  equ     $fe
IO_TIMEX                equ     $ff

; Audio
IO_AUDIO                equ     $bffd
IO_TURBO_SOUND          equ     $fffd
IO_SPECDRUM             equ     $df

; Input
IO_KEYBOARD             equ     $fe
IO_MOUSE_BTNS           equ     $fadf
IO_MOUSE_X              equ     $fbdf
IO_MOUSE_Y              equ     $ffdf
IO_JOY0                 equ     $1f
IO_JOY1                 equ     $37

;;----------------------------------------------------------------------------------------------------------------------
;; Next registers

REG_MACH_ID             equ     $00
REG_VER                 equ     $01
REG_RESET               equ     $02
REG_MACH_TYPE           equ     $03
REG_RAM_PAGE            equ     $04
REG_PERIPH_1            equ     $05
REG_PERIPH_2            equ     $06
REG_CLK_SPEED           equ     $07
REG_PERIPH_3            equ     $08
REG_PERIPH_4            equ     $09
REG_PERIPH_5            equ     $0a
REG_SUBVER              equ     $0e

REG_ANTI_BRICK          equ     $10
REG_VTIMING             equ     $11
REG_L2_PAGE             equ     $12
REG_L2_SHADOW           equ     $13
REG_TRANS_COL           equ     $14
REG_SPRITE_CTL          equ     $15
REG_L2_XLSB             equ     $16
REG_L2_Y                equ     $17
REG_CLIP_L2             equ     $18
REG_CLIP_SPR            equ     $19
REG_CLIP_ULA            equ     $1a
REG_CLIP_TMAP           equ     $1b
REG_CLIP_CTL            equ     $1c
REG_VLINE_H             equ     $1e
REG_VLINE_L             equ     $1f

REG_LN_INT_CTL          equ     $22
REG_LN_INT_VAL          equ     $23
REG_ULA_X               equ     $26
REG_ULA_Y               equ     $27
REG_KMAP_ADDRH          equ     $28
REG_KMAP_ADDRL          equ     $29
REG_KMAP_DATAH          equ     $2a
REG_KMAP_DATAL          equ     $2b
REG_DAC_LEFT            equ     $2c
REG_DAC_MONO            equ     $2d
REG_DAC_RIGHT           equ     $2e
REG_TMAP_XMSB           equ     $2f

REG_TMAP_XLSB           equ     $30
REG_TMAP_Y              equ     $31
REG_LORES_X             equ     $32
REG_LORES_Y             equ     $33
REG_SPR_IDX             equ     $34
REG_SPR_0               equ     $35
REG_SPR_1               equ     $36
REG_SPR_2               equ     $37
REG_SPR_3               equ     $38
REG_SPR_4               equ     $39

REG_PAL_INDEX           equ     $40
REG_PAL_VAL8            equ     $41
REG_ULAN_MASK           equ     $42
REG_PAL_CTL             equ     $43
REG_PAL_VAL16           equ     $44
REG_FBACK_COL           equ     $4a
REG_SPR_TRANS           equ     $4b
REG_TMAP_TRANS          equ     $4c

REG_MMU0                equ     $50
REG_MMU1                equ     $51
REG_MMU2                equ     $52
REG_MMU3                equ     $53
REG_MMU4                equ     $54
REG_MMU5                equ     $55
REG_MMU6                equ     $56
REG_MMU7                equ     $57

REG_COP_DATA8           equ     $60
REG_COP_CTL_L           equ     $61
REG_COP_CTL_H           equ     $62
REG_COP_DATA16          equ     $63
REG_VLINE_OFF           equ     $64
REG_ULA_CTL             equ     $68
REG_DISP_CTL            equ     $69
REG_LORES_CTL           equ     $6a
REG_TMAP_CTL            equ     $6b
REG_TMAP_ATTR           equ     $6c
REG_TMAP_BASE           equ     $6e
REG_TILES_BASE          equ     $6f

REG_L2_CTL              equ     $70
REG_L2_XMSB             equ     $71
REG_SPR_0_INC           equ     $75
REG_SPR_1_INC           equ     $76
REG_SPR_2_INC           equ     $77
REG_SPR_3_INC           equ     $78
REG_SPR_4_INC           equ     $79

;;----------------------------------------------------------------------------------------------------------------------
;; esxDOS compatible API calls
;; In DOT commands, use RST $08 to call these.

DISK_FILEMAP    equ     $85
DISK_STRMSTART  equ     $86
DISK_STRMEND    equ     $87

M_DOSVERSION    equ     $88
M_GETSETDRV     equ     $89
M_TAPEIN        equ     $8b
M_TAPEOUT       equ     $8c
M_GETHANDLE     equ     $8d     ; Exit: A=handle, CF=0
M_GETDATE       equ     $8e
M_EXECCMD       equ     $8f
M_SETCAPS       equ     $91
M_DRVAPI        equ     $92
M_GETERR        equ     $93
M_P3DOS         equ     $94     ; +3 DOS function call
M_ERRH          equ     $95

F_OPEN          equ     $9a     ; Entry: A=drive, HL=filespec, B=Access mode, Exit: CF=0: A=handle, CF=1: A=error code
F_CLOSE         equ     $9b
F_SYNC          equ     $9c
F_READ          equ     $9d     ; Entry: A=handle, HL=address, BC=bytes, Exit: BC=bytes read, CF=0: HL=after bytes, CF=1: A=error code
F_WRITE         equ     $9e
F_SEEK          equ     $9f
F_GETPOS        equ     $a0
F_FSTAT         equ     $a1
F_FTRUNCATE     equ     $a2
F_OPENDIR       equ     $a3
F_READDIR       equ     $a4
F_TELLDIR       equ     $a5
F_SEEKDIR       equ     $a6
F_REWINDDIR     equ     $a7
F_GETCWD        equ     $a8
F_CHDIR         equ     $a9
F_MKDIR         equ     $aa
F_RMDIR         equ     $ab
F_STAT          equ     $ac
F_UNLINK        equ     $ad 
F_TRUNCATE      equ     $ae
F_CHMOD         equ     $af
F_RENAME        equ     $b0
F_GETFREE       equ     $b1

dos             macro   func
                rst     8
                db      func
                endm

FA_READ                 equ     $01
FA_WRITE                equ     $02
FA_RWP3HDR              equ     $40     ; Include +3 dos header.
FA_OPEN_EXISTING        equ     $00     ; Open an existing file, but error if not existing.
FA_OPEN                 equ     $08     ; Open an existing file or create a new one.
FA_CREATE               equ     $04     ; Create a new file or error if it already exists.
FA_CREATE_NEW           equ     $0c     ; Create a new file, deleting it if it already exists.

IDE_BANK                equ     $01bd   ; NextZXOS function to manage memory

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
