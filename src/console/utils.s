;;----------------------------------------------------------------------------------------------------------------------
;; Utilities
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; 16-bit compare

compare16:
        ; Input:
        ;       HL = 1st value
        ;       DE = 2nd value
        ; Output:
        ;       CF, ZF = results of comparison:
        ;
        ;               CF      ZF      Result
        ;               -----------------------------------
        ;               0       0       HL > DE
        ;               0       1       HL == DE
        ;               1       0       HL < DE
        ;               1       1       Impossible
        ;
                push    hl
                and     a
                sbc     hl,de
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; max

max:
        ; Input:
        ;       HL = 1st value
        ;       DE = 2nd value
        ; Output:
        ;       HL = maximum value
        ;       DE = minimum value
        ;       CF = 1 if DE was maximum
        ;
                and     a
                sbc     hl,de
                jr      c,.choose_2nd   ; HL < DE?  Choose DE!
                add     hl,de           ; Restore HL
                ret
.choose_2nd     add     hl,de
                ex      de,hl
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; bitSet
;; Set a bit in a bit array (maximum length = 256 bits).
;;
;; Input:
;;      BC = bit array
;;      A = bit index
;;

bitSet:
                push    af,de,hl
                ld      d,0
                ld      e,a
                pixelad
                add     hl,bc
                add     hl,-$4000
                setae
                or      (hl)
                ld      (hl),a
                pop     hl,de,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; bitRes
;; Set a bit in a bit array (maximum length = 256 bits).
;;
;; Input:
;;      BC = bit array
;;      A = bit index
;;

bitRes:
                push    af,de,hl
                ld      d,0
                ld      e,a
                pixelad
                add     hl,bc
                add     hl,-$4000
                setae
                cpl
                and     (hl)
                ld      (hl),a
                pop     hl,de,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; bitGet
;; Fetch the result of a bit from a bit array (maximum length = 256 bits)
;;
;; Input:
;;      BC = bit array
;;      A = bit index
;;
;; Output:
;;      A = 0 for 0 bit, or non-zero for 1 bit
;;      ZF = 1 if bit is 0
;;

bitGet:
                push    de,hl
                ld      d,0
                ld      e,a
                pixelad
                add     hl,bc
                add     hl,-$4000
                setae
                and     (hl)
                pop     hl,de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; alignUp8K
;; align a 16-bit value up to the nearest 8K
;;
;; Input:
;;      HL = value
;;
;; Output:
;;      HL = aligned value

alignUp8K:
                push    af
                ld      a,l
                and     a               ; LSB == 0?
                jr      nz,.alignup
                ld      a,h
                and     $1f
                jr      nz,.alignup

                ; Value is already aligned
.end            pop     af
                ret

.alignup:
                ld      l,0
                ld      a,h
                and     $e0
                add     a,$20
                ld      h,a
                jr      .end


;;----------------------------------------------------------------------------------------------------------------------
;; moreBytes
;; Make some room in a buffer by pushing it up.  Any data pushed off the end is deleted
;;
;; Input:
;;      HL = buffer
;;      BC = size of buffer
;;      DE = number of bytes to push
;;

moreBytes:
                ; Calculate size of bytes to copy (size of buffer - number bytes to push)
                push    bc,de,hl
                and     a               ; Clear CF
                push    hl
                push    bc
                pop     hl              ; HL = size of buffer
                sbc     hl,de           ; Calculate number of bytes to copy
                push    hl
                pop     bc              ; Put value in BC
                pop     hl              ; Restore source of copy

                ; Calculate destination of copy
                ex      de,hl
                add     hl,de           ; HL = buffer start + number of bytes to push
                ex      de,hl           ; DE = destination of copy
                call    memcpy_r

                ; Zero start
                pop     hl,bc           ; HL = buffer, BC = number of bytes inserted
                xor     a
                call    memfill
                ld      de,bc
                pop     bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; lessBytes
;; Delete beginning of buffer, filling the area with the following data.  Data at end of buffer is zeroed.
;;
;; Input:
;;      HL = buffer
;;      BC = size of buffer
;;      DE = number of bytes to delete
;;

lessBytes:
                push    bc,de,hl

                push    de              ; Store number of bytes

                ex      de,hl           ; HL = num bytes to delete, DE = buffer
                add     hl,de           ; HL = source point

                push    hl              ; Store source point
                push    bc
                pop     hl              ; HL = size of buffer
                and     a
                sbc     hl,de           ; HL = number of bytes to copy
                push    hl
                pop     bc
                pop     hl              ; Restore source point

                call    memcpy

                ; Zero the following bytes
                ex      de,hl           ; HL = start of buffer
                add     hl,bc           ; HL = area to zero
                pop     bc              ; BC = number of bytes to zero
                xor     a
                call    memfill
                pop     hl,de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
