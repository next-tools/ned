;;----------------------------------------------------------------------------------------------------------------------
;; Memory routines
;;----------------------------------------------------------------------------------------------------------------------

;                DEFINE MEM_DEBUG

DivMMCPage:     db      0               ; Stores the divMMC register state

;;----------------------------------------------------------------------------------------------------------------------
;; DMA memory routines
;;----------------------------------------------------------------------------------------------------------------------


memcpy_dma:
                db      %11000011       ; R6: Reset

                ; Register 0 set up
                db      %01111101       ; R0: A -> B, transfer mode
mc_dma_src:     dw      0               ; Source address
mc_dma_len:     dw      0               ; Length

                ; Register 1 set up (Port A configuration)
mc_dma_dir:     db      %01010100       ; R1: Port A config: increment, variable timing
                db      2               ; R1: Cycle length of 2 in port A

                ; Register 2 set up (Port B configuration)
mc_dma_dir2:    db      %01010000       ; R2: Port B config: address fixed, variable timing
                db      2               ; R2: Cycle length of 2 in port B

                ; Register 4 set up (Operation mode)
                db      %10101101       ; R4: Continuous mode, set destination address
mc_dma_dest:    dw      0               ; Destination address

                ; Register 5 set up (Some control)
                db      %10000010       ; R5: Stop at end of block; read active low

                ; Register 6 (Commands)
                db      %11001111       ; R6: Load
                db      %10000111       ; R6: Enable DMA


memcpy_len      EQU     $-memcpy_dma

memcpy:
        ; Input:
        ;       HL = source address
        ;       DE = destination address
        ;       BC = number of bytes
        ;
                push    af
                ld      a,b
                or      c
                jr      nz,.some_bytes
                pop     af
                ret
.some_bytes:
                push    bc
                push    de
                push    hl
                ld      a,%01010100
                ld      (mc_dma_dir),a          ; Set direction
                ld      a,%01010000
                ld      (mc_dma_dir2),a

transfer:
                ld      (mc_dma_src),hl         ; Set up the source address
                ld      (mc_dma_len),bc         ; Set up the length
                ld      (mc_dma_dest),de        ; Set up the destination address
                ld      hl,memcpy_dma
                ld      b,memcpy_len
                ld      c,$6b
                otir                            ; Send DMA program
                pop     hl
                pop     de
                pop     bc
                pop     af
                ret

memcpy_r:
        ; Input:
        ;       HL = source address
        ;       DE = destination address
        ;       BC = number of bytes
        ;
                push    af
                ld      a,b
                or      c
                jr      nz,.some_bytes
                pop     af
                ret
.some_bytes:
                push    bc
                push    de
                push    hl

                ; Prepare the proper HL & DE
                add     hl,bc
                ex      de,hl
                add     hl,bc
                ex      de,hl
                dec     hl
                dec     de

                ; Set up the DMA
                ld      a,%01000100
                ld      (mc_dma_dir),a
                ld      a,%01000000
                ld      (mc_dma_dir2),a
                jr      transfer

memmove:
        ; Input:
        ;       HL = source address
        ;       DE = destination address
        ;       BC = number of bytes
        ;
        ; Output:
        ;       CF = 0: normal copy, 1: reverse copy
        ;
        ; Destroys:
        ;       AF
        ;
                call    max
                jr      nc,memcpy
                ex      de,hl
                jr      memcpy_r

memfill:
        ; Input:
        ;       HL = source address
        ;       BC = number of bytes
        ;       A = value to fill
        ;
        ; This is equivalent to memcpy(HL,HL+1,BC-1), when we load (HL) with A.
        ;
                push    af
                ld      a,b
                or      c
                jr      z,.done
                pop     af
                push    af
                push    de
                ld      (hl),a
                ld      d,h
                ld      e,l
                inc     de
                dec     bc
                call    memcpy
                inc     bc              ; Restore BC
                pop     de
.done           pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Internal routine to allocate a single page from the OS.
;;
;; Output:
;;      A = page # (or 0 if failed)
;;      CF = 1 if out of memory
;;
;;----------------------------------------------------------------------------------------------------------------------

allocPage:      
                push    ix
                push    bc
                push    de
                push    hl

                ; Allocate a page by using the OS function IDE_BANK.
                ld      hl,$0001        ; Select allocate function and allocate from normal memory.
                exx                     ; Function parameters are switched to alternative registers.
                ld      de,IDE_BANK     ; Choose the function.
                ld      c,7             ; We want RAM 7 swapped in when we run this function (so that the OS can run).
                rst     8
                db      M_P3DOS         ; Call the function, new page # is in E
                jr      c,.success

                ; We failed here
                xor     a
                ld      e,a             ; Page # is 0 (i.e. error)

.success        ld      a,e
                ccf
                
        IFDEF MEM_DEBUG
                push    af
                ld      hl,PagesAllocd
                add     hl,a
                ld      (hl),1
                pop     af
        ENDIF

                pop     hl
                pop     de
                pop     bc
                pop     ix
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Internal routine to return a previously allocated page back to the OS.
;;
;; Input:
;;      A = page #
;;
;;----------------------------------------------------------------------------------------------------------------------

freePage:       
                push    af
                push    ix
                push    bc
                push    de
                push    hl

        IFDEF MEM_DEBUG
                ld      hl,PagesAllocd
                add     hl,a
                ld      (hl),0
        ENDIF

                ld      e,a             ; E = page #
                ld      hl,$0003        ; Deallocate function from normal memory
                exx                     ; Function parameters are switched to alternative registers.
                ld      de,IDE_BANK     ; Choose the function.
                ld      c,7
                rst     8
                db      M_P3DOS

                pop     hl
                pop     de
                pop     bc
                pop     ix
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; maxPage
;; Return the # of the highest page in memory.  Requires sysvars to be available.
;;
;; Output:
;;      A = highest page
;;

;maxPage:
;                push    bc,hl
;
;                ; Store the state of MMU7
;                rreg    NR_MMU7
;                ld      c,a
;
;                ; Try the highest page on a 2MB system
;                ld      a,$df
;                page    7,a
;                ld      hl,$c000
;                ld      b,(hl)          ; Store original byte
;
;                ld      a,%10101010
;                ld      (hl),a
;                ld      a,(hl)
;                cp      %10101010
;                jr      nz,.not_2mb
;
;                ld      (hl),b
;                ld      a,(hl)
;                cp      b
;                ld      a,$df
;                jr      z,.done
;.not_2mb:
;                ld      a,$5f
;.done:
;                ld      b,a
;                ld      a,c
;                page    7,a
;                ld      a,b
;                pop     hl,bc
;                ret

maxPage:
                ld      a,($5b69)
                add     a,a
                inc     a
                ret


;;----------------------------------------------------------------------------------------------------------------------
;; Memory leak detection

        IFDEF MEM_DEBUG

                display "MEMORY LEAK DETECTION ON..."

PagesAllocd:    ds      256,0

DetectLeaks     macro
                ld      hl,PagesAllocd
                break
                endm

        ELSE

DetectLeaks     macro
                endm

        ENDIF

;;----------------------------------------------------------------------------------------------------------------------
;; pageVideo
;; Make sure the tilemap is available

pageVideo:
                page    2,10
                page    3,11
                ret

