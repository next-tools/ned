;;----------------------------------------------------------------------------------------------------------------------
;; Buffer manager
;;----------------------------------------------------------------------------------------------------------------------

NUM_BUFFERS     equ     32

;;----------------------------------------------------------------------------------------------------------------------
;; Buffer structure
;;----------------------------------------------------------------------------------------------------------------------

                struct  Buffer

Pages           ds      8               ; Page indices that make up the doc
Size            dw      0               ; Size of buffer in bytes
GapStart        dw      0               ; Offset of start of gap
GapEnd          dw      0               ; Offset of end of gap

                ends

buffer_start    equ     $

Buffers         ds      NUM_BUFFERS*Buffer,0    ; Support for NUM_BUFFERS in-memory buffers
BufferSwap      ds      Buffer,0                ; Area for helping with swapping buffers

buffer_len      equ     $ - buffer_start

;;----------------------------------------------------------------------------------------------------------------------
;; buffersInit
;; Initialises the buffer system
;;----------------------------------------------------------------------------------------------------------------------

buffersInit:
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; buffersDone
;; Deallocates all memory related to buffers
;;----------------------------------------------------------------------------------------------------------------------

buffersDone:
                ld      hl,Buffers + Buffer.Pages
                ld      de,Buffer
                ld      c,NUM_BUFFERS
.l0:
                ; Loop through each buffer
                ld      b,8
                push    hl
.l1:
                ; Loop through each page in the buffer
                ldi     a,(hl)          ; Fetch the page #
                and     a
                jr      z,.done         ; Finished all pages in this doc?
                call    freePage
                djnz    .l1
.done:
                pop     hl
                add     hl,de
                dec     c
                jr      nz,.l0

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; bufferLoad
;; Create a new buffer based on a file
;;
;; Input:
;;      HL = filename
;;
;; Output:
;;      CF = 0 if successful

bufferLoad:
                call    bufferObtain            ; Start creation of new buffer - it will be in index 0
                ret     c                       ; TODO: Error message

                di
                push    bc,de,hl

                ; Get the default drive
                xor     a
                rst     8
                db      $89

                ; Open the file
                break
                ld      b,FA_READ
                dos     F_OPEN
                and     a
                jp      z,.fail_open
                ld      (.BufferHandle),a

                ; Get information on the file
                ld      ix,.FileInfo
                push    ix
                pop     hl
                dos     F_FSTAT
                jp      c,.fail_stat
                ld      hl,(.BufferSizeH)
                ld      a,h
                or      l
                jp      nz,.fail_stat           ; File is too large (max size is 64K)

                ; Loop and load in the text file 8K (or less) at a time
                ld      hl,Buffers
                ld      bc,(.BufferSize)
                ld      (.BufferSizeH),bc       ; Reuse for storing original size
.l0:
                ld      bc,(.BufferSize)
                ld      a,b
                or      c
                jr      z,.done                 ; We've finished loading whole file

                call    allocPage               ; Allocate an 8K page
                jr      c,.fail_nomem           ; Out of memory!
                ldi     (hl),a                  ; Store page
                page    6,a

                ld      a,b
                and     $e0                     ; >= 8K?
                jr      z,.small

                ; Load an entire 8K
                ld      bc,$2000
.small: 
                and     a                       ; Clear CF
                ld      hl,(.BufferSize)
                sbc     hl,bc
                ld      (.BufferSize),hl        ; Update amount of bytes left to load
                ld      hl,$c000                ; Load data into MMU6
                ld      a,(.BufferHandle)
                dos     F_READ
                jr      .l0
.done:
                ld      a,(.BufferHandle)
                dos     F_CLOSE

                ; Update the buffer meta data
                ld      ix,Buffers
                ld      hl,(.BufferSizeH)       ; HL = size of file
                ld      (ix+Buffer.GapStart),hl
                call    alignUp8K               ; HL = size aligned with pages
                ld      (ix+Buffer.GapEnd),hl
                ld      (ix+Buffer.Size),hl


                jr      .finish

.fail_nomem:
                ; TODO status error message
                call    bufferKill
                jr      .cont1
.fail_stat:
                ; TODO status error message
.cont1:
                ld      a,(.BufferHandle)
                dos     F_CLOSE
                jr      .cont2
.fail_open:
                ; TODO status error message
.cont2:
                xor     a
                scf
.finish:
                pop     hl,de,bc
                ei
                ret

.FileInfo       ds      7
.BufferSize     dw      0
.BufferSizeH    ds      2
.BufferHandle   db      0

;;----------------------------------------------------------------------------------------------------------------------
;; bufferNew
;; Create a brand new buffer
;;

bufferNew:
                call    bufferObtain
                ret     c                       ; TODO: Error message

                di
                call    allocPage
                jr      c,.fail_nomem

                ld      ix,Buffers
                ld      (ix+0),a                ; Store first page
                page    6,a                     ; Page it in
                ld      a,$20
                ld      (ix+Buffer.Size+1),a
                ld      (ix+Buffer.GapEnd+1),a

                ei
                ret

.fail_nomem:
                ; TODO: Out of memory status error message
                call    bufferKill
                ei
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; bufferObtain
;; Create a new buffer entry.
;;
;; Output:
;;      CF = 1 if too many buffers
;;

bufferObtain:
                push    bc,de,hl
                ld      hl,Buffers+(Buffer*(NUM_BUFFERS-1))     ; Position of last buffer slot
                ld      a,(hl)
                and     a
                scf
                jr      nz,.done                                ; Error if no buffer slots left

                ld      hl,Buffers
                ld      bc,(NUM_BUFFERS+1)*Buffer
                ld      de,Buffer
                call    moreBytes

                and     a
.done:
                pop     hl,de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; bufferKill
;; Destroy the current buffer
;;

bufferKill:
                ld      hl,Buffers
                ld      bc,(NUM_BUFFERS)*Buffer
                ld      de,Buffer
                call    lessBytes
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------

