;;----------------------------------------------------------------------------------------------------------------------
;; Screen management
;;----------------------------------------------------------------------------------------------------------------------

StatusLineLength        db      0

;;----------------------------------------------------------------------------------------------------------------------
;; statusSet
;; Set the message at the bottom of the screen
;;
;; Input:
;;      HL = Null-terminated text
;;

statusSet:
                push    hl

                ; Store the current cursor position
                call    consoleStore

                ; Output the status line at the correct position
                call    cursorHide
                ld      c,0
                ld      b,kScreenHeight-1
                call    at
                ld      a,kInkStatus
                call    setColour
                ld      b,79
                pop     hl
                call    printHLWidth
                ld      a,C_CLEARTOEND
                call    printChar

                ; Restore everything
                call    consoleRestore
                ret

