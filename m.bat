@echo off
env\sjasmplus\sjasmplus src\main.s -Ienv/src --zxnext=cspect --syntax=m --msg=war --fullpath --lst="ned.lst"
if not errorlevel 1 (
    if not exist _temp (
    	mkdir _temp
    	copy env\tbblue.mmc _temp
    )
    env\hdfmonkey\hdfmonkey put _temp\tbblue.mmc ned /dot
    env\hdfmonkey\hdfmonkey put _temp\tbblue.mmc data/test.txt /
    
    if exist d:\nextzxos (
        copy ned d:\dot
    )

    if exist n:\sync\server (
        copy ned n:\sync\server\dot
    )
)

